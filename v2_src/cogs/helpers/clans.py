class Clan:
    """
    A class to store a single clan
    """

    def __init__(self, clan_id: str, name: str, role: int, leaders = []):
        self.clan_id = clan_id
        self.name = name
        self.role = role
        self.leaders = []

clans = [
    Clan("bratva",  "Bratva",  473221292368330752),
    Clan("yakuza",  "Yakuza",  473221597054894081),
    Clan("triad",   "Triad",   473221404213379072),
    Clan("camorra", "Camorra", 473221719528701956)
]