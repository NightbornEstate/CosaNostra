import discord, datetime
import time, asyncio
from discord.ext import commands
from .helpers.relativeDates import timesince
from .helpers.clans import clans

def pretty_time_delta(seconds):
    seconds = int(seconds) + 1
    days, seconds = divmod(seconds, 86400)
    hours, seconds = divmod(seconds, 3600)
    minutes, seconds = divmod(seconds, 60)
    if days > 0:
        return '%dd%dh%dm%ds' % (days, hours, minutes, seconds)
    elif hours > 0:
        return '%dh%dm%ds' % (hours, minutes, seconds)
    elif minutes > 0:
        return '%dm%ds' % (minutes, seconds)
    else:
        return '%ds' % (seconds,)

class War:
    def get_clan(self, m: discord.Member):
        """
        Returns the clan of a member.
        """

        for clan in clans:
            if clan.role in [r.id for r in m.roles]:
                return clan
    
    def __init__(self, message: discord.Message):
        self.message = message
        self.length = 10 * 60 # 10 minutes
        self.signups = {}
        self.started = time.time()
        for clan in clans:
            self.signups[clan.clan_id] = []

    async def update_embed(self):
        embed = discord.Embed(
            title = "Mafia War started:exclamation:",
            description = "Type `!fight` into <#433033436035285009> to fight for your clan, or `!war` to see what this game is about!",
            color = discord.Color(0x7dcd85)
        ).set_thumbnail(
            url = "https://cdn.discordapp.com/attachments/434787402725326850/485209981327638580/image0.png"
        )
        # Total the amount of votes, for the percentage calc.
        total = sum([len(x) for x in list(self.signups.values())])

        for clan in clans:
            embed.add_field(
                name = f"{clan.name} Fighters",
                value = f"{len(self.signups[clan.clan_id])} fighters, {(round( (len(self.signups[clan.clan_id]) / total) * 100 )) if total > 0 else 0}% of army"
            )

        embed.add_field(
            name = "Remaining Time",
            value = pretty_time_delta((self.started + self.length) - time.time()),
            inline = False
        )
        await self.message.edit(embed = embed)
        


    def signup_member(self, member: discord.Member):
        clan = self.get_clan(member)
        self.signups[clan.clan_id] += [member]


    async def do_award(self):
        _self = self
        winning_clan = max(list(self.signups), key=(lambda(sup): len(_self[sup])))
        members = self.signups[winning_clan]


    async def wait_for_finish(self):
        for i in range(self.length // 120):
            await self.update_embed()
            print(self.length // 120)
            t = time.time()
            await asyncio.sleep(self.length // 120)
            
        await self.do_award()


class FightCog:
    def __init__(self, bot):
        self.bot = bot
        self.war_active = None

    @commands.command()
    @commands.guild_only()
    async def fight(self, ctx):
        if not self.war_active:
            await ctx.send(
                embed = discord.Embed(
                    name = "<a:jellywo:477207966634737674> There isn't a war at the moment",
                    color = ctx.message.author.color,
                    description = f"Bug your <@&433693780781039626> :)"
                )
            )
            return

        self.war_active.signup_member(ctx.message.author)
        await ctx.send(
            embed = discord.Embed(
                title = "<a:jellywo:477207966634737674> Signed you up",
                color = ctx.message.author.color,
                description = f"Thanks for signing up! Make sure to ping your friends because WE NEED **YOU**!"
            ).set_thumbnail(
                url = "http://2.bp.blogspot.com/-JQF068-6Yas/VViutKZhJRI/AAAAAAABWbg/eImtoyLVjAU/s1600/Britain%2Bshall%2Bnot%2Bburn%2B(UK).jpg"
            )
        )

    @commands.command()
    @commands.guild_only()
    @commands.cooldown(1, float(60 * 60 * 6)) # 6 hour cooldown
    @commands.has_role("Clan Leader")
    async def mafiawar(self, ctx):
        # Send to announcements
        announcements = ctx.message.guild.get_channel(433096845934198789)

        m = await announcements.send(
            embed = discord.Embed(
                title = "Mafia War started:exclamation:",
                description = "Type `!fight` into <#433033436035285009> to fight for your clan, or `!war` to see what this game is about!",
                color = discord.Color(0x7dcd85)
            ).set_thumbnail(url = "https://cdn.discordapp.com/attachments/434787402725326850/485209981327638580/image0.png")
        )
        print(m)
        await ctx.message.add_reaction("👌")
        war = War(m)
        self.war_active = war
        await war.wait_for_finish()
        self.war_active = None
    
    @commands.command()
    @commands.guild_only()
    async def war(self, ctx):
        await ctx.send("This game can be started by clan leaders once every 6 hours. The server members have 10 minutes to `!fight`, and the total number of people who ran the command from each clan is updated in #announcements. Once 10 minutes is up, the members who used !fight from the clan where the most members signed up recieve 30k souls each!.")

    async def on_command_error(self, ctx, err):
        if isinstance(err, commands.CommandNotFound):
            return

        if isinstance(err, commands.CommandOnCooldown):
            retry = err.retry_after
            await ctx.send(
                embed = discord.Embed(
                    title = "<:drakeno:433041256122417162> The cooldown is still cooling down",
                    color = ctx.message.author.color,
                    description = f"You'll be able to use this in {pretty_time_delta(retry)} :-)"
                )
            )
            return
        
        if isinstance(err, commands.CheckFailure):
            await ctx.send(
                embed = discord.Embed(
                    title = "<:drakeno:433041256122417162> You don't have permission for that",
                    description = type(err).__name__ + ": " + str(err),
                    color = ctx.message.author.color
                )
            )
            return

        if (ctx.command.name == "mafiawar") and isinstance(err, commands.CommandInvokeError) and isinstance(err.original, discord.NotFound):
            await ctx.send(f"<@{ctx.message.author.id}>",
                embed = discord.Embed(
                    title = "<:missing:433166938626326538> Someone deleted the message from announcements, smh",
                    description = "Start another mafia war in a few hours if you want!",
                    color = ctx.message.author.color
                )
            )
            return
            
        if isinstance(err, commands.CommandInvokeError):
            e = err.original
            await ctx.send("<@193053876692189184> halp!",
                embed = discord.Embed(
                    title = "<:drakeno:433041256122417162> Command failed",
                    description = type(e).__name__ + ": " + str(e),
                    color = ctx.message.author.color
                )
            )
            return
            
        
        raise err


def setup(bot):
    bot.add_cog(FightCog(bot))