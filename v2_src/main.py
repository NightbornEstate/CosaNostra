from discord.ext import commands
import sys, traceback, discord

with open("token") as f:
    token = f.read()

bot = commands.Bot(command_prefix='$')

initial_extensions = [
    "cogs.fight"
]

if __name__ == '__main__':
    for extension in initial_extensions:
        try:
            bot.load_extension(extension)
        except Exception as e:
            print(f'Failed to load extension {extension}.', file=sys.stderr)
            traceback.print_exc()

bot.run(token)