const clans = require("../data/clans.json");
const discord = require("discord.js");
const announcements = "433096845934198789";
//const announcements = "485472830130225184";

module.exports = async function (message, message_id, redis) {
    var clanFighters = {};
    for (var clan_i in clans) {
        var clan = clans[clan_i]
        clanFighters[clan.name] = await redis.llenAsync("cosa:mafiawar:" + clan.name + ":signups")
    }

    var total = 0;
    clans.forEach(clan => { total += clanFighters[clan.name] });

    if (total === 0) {
        var clanPercents = {};
        clans.forEach( clan => {
            clanPercents[clan.name] = 0
        })
    } else {
        var clanPercents = {};
        clans.forEach(clan => {
            var fighters = clanFighters[clan.name]
            if (fighters === 0) clanPercents[clan.name] = 0;
            else { clanPercents[clan.name] = fighters / total }
        })
    }
    
    var highest = 0;
    var highest_clan = "Yakuza";

    for (var clan_i in clans) {
        var clan = clans[clan_i];
        if (highest <= clanPercents[clan.name]) {
            highest = clanPercents[clan.name]
            highest_clan = clan
        }
    }

    var emb = new discord.RichEmbed()
        .setTitle(clan.name + " Wins!")
        .setDescription("All members who used the !fight command from that clan recieved 30k souls O-O. Check back next time to have a chance to win in the brawl!")
        .addField("Number of Members", Math.floor(highest * 100) + "% of the people who used !fight were members of " + highest_clan.name)
        .setColor(message.guild.roles.get(clan.role).color)

    await (await message.guild.channels.get(announcements).fetchMessage(message_id)).edit(emb)


}