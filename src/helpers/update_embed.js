const discord = require("discord.js");
const moment = require("moment");
const clans = require("../data/clans.json");
const announcements = "433096845934198789";

async function update(message, message_id, redis, make_new, mytaskid) {
    var announceEmb = new discord.RichEmbed()
        .setColor(0x7dcd85)
        .setTitle("Mafia War started:exclamation:")
        .setDescription("Type `!fight` into <#433033436035285009> to fight for your clan, or `!war` to see what this game is about!")
        .setThumbnail("https://cdn.discordapp.com/attachments/434787402725326850/485209981327638580/image0.png")
        .setFooter("0 fighters total")


    var clanFighters = {};
    for (var clan_i in clans) {
        var clan = clans[clan_i]
        clanFighters[clan.name] = await redis.llenAsync("cosa:mafiawar:" + clan.name + ":signups")
    }

    var total = 0;
    clans.forEach(clan => { total += clanFighters[clan.name] });

    if (total === 0) {
        var clanPercents = {};
        clans.forEach( clan => {
            clanPercents[clan.name] = 0
        })
    } else {
        var clanPercents = {};
        clans.forEach(clan => {
            var fighters = clanFighters[clan.name]
            if (fighters === 0) clanPercents[clan.name] = 0;
            else { clanPercents[clan.name] = fighters / total }
        })
    }
  
    clans.forEach((clan) => {
        announceEmb.addField(clan.name + " Fighters", `${clanFighters[clan.name]} fighters, ${Math.round(clanPercents[clan.name] * 100)}% total`)
    })

    announceEmb.addBlankField()
    announceEmb.addField("Remaining time:", moment().add(await redis.ttlAsync("cosa:mafiawar:active"), "s").fromNow().replace(/in /g, ""))
    if ((await redis.ttlAsync("cosa:mafiawar:active")) < 10) {
        clearInterval(mytaskid)
        require("./finish_war.js")(message, message_id, redis)
    }
    if (make_new) {
        return await message.guild.channels.get(announcements).send(announceEmb)
    } else {
        return await (await message.guild.channels.get(announcements).fetchMessage(message_id)).edit(announceEmb)
    }
}

module.exports = update