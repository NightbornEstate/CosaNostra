const clans = require("../data/clans.json")
function determine_clan(member) {
    for (var clan_i in clans) {
        var clan = clans[clan_i];
        if (member.roles.has(clan.role)) return clan.name
    }
}
module.exports = determine_clan