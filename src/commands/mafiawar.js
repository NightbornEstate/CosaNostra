const { Command } = require('discord-akairo');
const discord = require("discord.js");
const moment = require("moment");
const clans = require("../data/clans.json");
const announcements = "433096845934198789";
const update = require("../helpers/update_embed")

class MafiaWarCommand extends Command {
    constructor() {
        super('mafiawar', {
           aliases: ['mafiawar'] 
        });
    }

    async exec(message) {
        if (!message.member.roles.has("433693780781039626")) return message.reply(
            new discord.RichEmbed()
            .setColor(0xdb9d47)
            .setTitle("<a:wut:439279218371592192> Wut? This is a clan leader only command")
            .setDescription("Sorry!")
        )
        if (!(await message.client.redis.getAsync("cosa:mafiawar:cooldown"))) {
            message.client.redis.set("cosa:mafiawar:cooldown", "1")
            message.client.redis.expire("cosa:mafiawar:cooldown", 60 * 60 * 12)

            message.client.redis.set("cosa:mafiawar:active", "1")
            message.client.redis.expire("cosa:mafiawar:active", 60 * 10)
            
            var m = await update(message, null, message.client.redis, true)

            var taskId = setInterval( () => {
                update(message, m.id, message.client.redis, false, taskId)
            }, 5000)

            return message.reply('Started!');
        }
        return message.reply(
            new discord.RichEmbed()
            .setColor(0xdb9d47)
            .setTitle(":clock1030: This command is on cooldown, speedy")
            .setDescription("You'll be able to use this command again " + 
                moment().add(await message.client.redis.ttlAsync("cosa:mafiawar:cooldown"), "s").fromNow())
        )
        
    }
}

module.exports = MafiaWarCommand;