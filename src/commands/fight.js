const { Command } = require('discord-akairo');
const discord = require("discord.js");
const moment = require("moment");
const clans = require("../data/clans.json");
const announcements = "485472830130225184";
const update = require("../helpers/update_embed")
const determine_clan = require("../helpers/determine_clan")

class MafiaWarCommand extends Command {
    constructor() {
        super('fight', {
           aliases: ['fight']
        });
    }

    async exec(message) {
        if ((await message.client.redis.getAsync("cosa:mafiawar:active"))) {
            var clan = determine_clan(message.member)
            if (!clan) {
                return message.reply(
                    new discord.RichEmbed()
                    .setColor(0xdb9d47)
                    .setTitle(":thinking: You need to be a member of a clan")
                    .setDescription("You'll be able to use this command once you've `!join`ed a clan.")
                )
            }
            if (!message.client.redis.exists("cosa:mafiawar:" + clan + ":signups")) {
                message.client.redis.lpush("cosa:mafiawar:" + clan + ":signups", message.author.id);
                message.client.redis.expire("cosa:mafiawar:" + clan + ":signups", await message.client.redis.ttlAsync("cosa:mafiawar:active"))
                message.client.redis.set("cosa:mafiawar:signedup_cache:" + message.member.id, "1")
                message.client.redis.expire("cosa:mafiawar:signedup_cache:" + message.member.id, await message.client.redis.ttlAsync("cosa:mafiawar:active"))
                return message.channel.send(
                    new discord.RichEmbed()
                    .setColor(0x7dcd85)
                    .setTitle("Joined the war:exclamation:")
                    .setDescription("You are supporting your clan!")
                )
            } else if (!(await message.client.redis.getAsync("cosa:mafiawar:signedup_cache:" + message.member.id))) {
                message.client.redis.lpush("cosa:mafiawar:" + clan + ":signups", message.author.id);
                message.client.redis.set("cosa:mafiawar:signedup_cache:" + message.member.id, "1")
                message.client.redis.expire("cosa:mafiawar:signedup_cache:" + message.member.id, await message.client.redis.ttlAsync("cosa:mafiawar:active"))
                return message.channel.send(
                    new discord.RichEmbed()
                    .setColor(0x7dcd85)
                    .setTitle("Joined the war:exclamation:")
                    .setDescription("You are supporting your clan!")
                )
            } else {
                return message.reply(
                    new discord.RichEmbed()
                    .setColor(0xdb9d47)
                    .setTitle(":<a:wut:439279218371592192> You've already signed up this war")
                    .setDescription("Thanks for supporting your clan!")
                )
            }
        }
        return message.reply(
            new discord.RichEmbed()
            .setColor(0xdb9d47)
            .setTitle(":thinking: There isn't a mafia war going on at the moment")
            .setDescription("You'll be able to use this command once a clan leader starts a `!mafiawar`.")
        )
        
    }
}

module.exports = MafiaWarCommand;