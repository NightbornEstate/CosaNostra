const { Command } = require('discord-akairo');

class PingCommand extends Command {
    constructor() {
        super('war', {
           aliases: ['war'] 
        });
    }

    exec(message) {
        return message.reply('This game can be started by clan leaders once every 12 hours. The server members have 10 minutes to `!fight`, and the total number of people who ran the command from each clan is updated in #announcements. Once 10 minutes is up, the members who used !fight from the clan where the most members signed up recieve 30k souls each!.');
    }
}

module.exports = PingCommand;