const Discord = require('discord.js');
const token = require("fs").readFileSync("./secrets/discord_token", "utf-8");
const { AkairoClient } = require('discord-akairo');
const redis = require('redis');
const bluebird = require("bluebird")
bluebird.promisifyAll(redis);
const rclient = redis.createClient();

const client = new AkairoClient({
    ownerID: '193053876692189184', // or ['123992700587343872', '86890631690977280']
    prefix: '!', // or ['?', '!']
    commandDirectory: './commands/'
}, {
    disableEveryone: true
});

client.on("ready", () => {
  console.log("Logged in as " + client.user.tag)
})

client.redis = rclient;

client.login(token);