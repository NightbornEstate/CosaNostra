cd src
docker build -t gcr.io/personal-storage-205017/cosa_nostra .
docker push gcr.io/personal-storage-205017/cosa_nostra

cd ..

kubectl apply -f cosa-nostra.yml
kubectl patch deployment cosa-nostra -p "{\"spec\":{\"template\":{\"metadata\":{\"labels\":{\"date\":\"`date +'%s'`\"}}}}}"
